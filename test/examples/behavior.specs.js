import sinon from 'sinon';

import * as face from '../../src/exercise-2/examples/face';
import mouth from '../../src/exercise-2/examples/mouth';

describe('exercise 4 - behavior test examples', () => {

    context('when using spies', () => {

        it('should be able to spy on dependency', () => {
            const soundAlarm = sinon.spy();
            face.wake(soundAlarm);
            soundAlarm.should.have.been.called;
        });

    });

    
    context('when using stub', () => {

        const artist = { applyLipstick: () => {} };
        const stub = sinon.stub(artist);
        stub.applyLipstick.returns('red');
        let color;

        before(() => {
            color = face.makeup(stub);
        });

        it('should still be able to spy on dependency', () => {
            stub.applyLipstick.should.have.been.calledWithExactly(mouth);
        });

        it('should return color applied', () => {
            color.should.equal('red');
        })

    });

    context('when using mocks', () => {

        const hooligan = { punch: () => {} };
        const mock = sinon.mock(hooligan);

        before(() => {
            mock.expects("punch").withArgs(mouth);
            face.punch(hooligan);
        });

        it('should punch mouth', () => {
            mock.verify();
        })

    });

});