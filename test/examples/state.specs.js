import Eye from '../../src/exercise-2/examples/Eye';

describe ('exercise 4 - state test examples', () => {

    describe('when creating an eye', () => {

        context('without side or color', () => {

            const sut = new Eye();
            
            before(() => {
                sut.close();
            });

            it('should default to center side', () => {
                sut.side.should.equal('center');
            });

            it('should default to green color', () => {
                sut.color.should.equal('green');
            });

            it('should be open', () => {
                sut.isOpen.should.be.false;
            });

        });

    });

    context('when closing an eye', () => {

        const sut = new Eye();
        
        before(() => {
            sut.close();
        });

        it('should be closed', () => {
            sut.isOpen.should.be.false;
        });

    });

    context('when opening an eye', () => {

        const sut = new Eye();
        
        before(() => {
            sut.close();
            sut.open();
        });

        it('should be open', () => {
            sut.isOpen.should.be.true;
        });

    });


})




