import sinon from 'sinon';

import tracker from '../../src/exercise-2/song-tracker';
import factory from '../builders/factory';

describe('exercise 2 - song tracker', () => {

  describe('when generating a report', () => {

    context('for a song tracked for the first time', () => {

      it('should indicate the song has been played once', () => {

        const song = factory.song.build();
        const display = sinon.spy();
        const expected = { [song.id]: 1 };

        tracker.track(song);
        tracker.generateReport(display);

        display.should.have.been.calledWithMatch(expected);

      });

    });

    context('for a song that has already been tracked', () => {

      it('should indicate the number of times the song has been played', () => {

        const song = factory.song.build();
        const display = sinon.spy();
        const expected = { [song.id]: 2 };

        tracker.track(song);
        tracker.track(song);
        tracker.generateReport(display);

        display.should.have.been.calledWithMatch(expected);

      });

    });

  });

});
