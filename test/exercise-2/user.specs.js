import sinon from 'sinon';

import factory from '../builders/factory';
import User from '../../src/exercise-2/user';

describe('exercise 2 - user', () => {

  describe('when getting favorite songs', () => {

    context('and the user only has a preferred genre', () => {

      let favorites = [];
      const preferences = factory.preferences.genre(factory.random.string()).build();
      const expected = factory.song.build();
      const stub = sinon.stub(factory.songs.shape());

      before(() => {
        stub.findByGenre.returns([expected]);
        const user = new User(preferences, stub);
        favorites = user.getFavoriteSongs();
      });

      it('should find songs by the preferred genre', () => {
        stub.findByGenre.should.have.been.calledWithExactly(preferences.genre);
      });

      it("should have only found songs related to the user's preferred genre", () => {
        favorites.should.deep.equal([expected]);
      });

      it('should not attempt to find songs by artist', () => {
        stub.findByArtist.should.not.have.been.called;
      });

    });

    context('and the user only has a preferred artist', () => {

      let favorites = [];
      const preferences = factory.preferences.artist(factory.random.string()).build();
      const expected = factory.song.build();
      const stub = sinon.stub(factory.songs.shape());

      before(() => {
        stub.findByArtist.returns([expected]);
        const user = new User(preferences, stub);
        favorites = user.getFavoriteSongs();
      });

      it('should find songs by the preferred artist', () => {
        stub.findByArtist.should.have.been.calledWithExactly(preferences.artist);
      });

      it("should have only found songs related to the user's preferred artist", () => {
        favorites.should.deep.equal([expected]);
      });

      it('should not attempt to find songs by genre', () => {
        stub.findByGenre.should.not.have.been.called;
      });

    });

    context('and the user has a preferred genre and artist', () => {

      let favorites = [];
      const preferences = factory
        .preferences
        .genre(factory.random.string())
        .artist(factory.random.string())
        .build();
      const expectedGenreSong = factory.song.build();
      const expectedArtistSong = factory.song.build();
      const stub = sinon.stub(factory.songs.shape());

      before(() => {
        stub.findByGenre.returns([expectedGenreSong]);
        stub.findByArtist.returns([expectedArtistSong]);
        const user = new User(preferences, stub);
        favorites = user.getFavoriteSongs();
      });

      it('should find songs by the preferred genre', () => {
        stub.findByGenre.should.have.been.calledWithExactly(preferences.genre);
      });

      it('should find songs by the preferred artist', () => {
        stub.findByArtist.should.have.been.calledWithExactly(preferences.artist);
      });

      it("should have found songs related to the user's preferred genre", () => {
        favorites.should.contain(expectedGenreSong);
      });

      it("should have found songs related to the user's preferred artist", () => {
        favorites.should.contain(expectedArtistSong);
      });

    });

    context('and the user has no preferences', () => {

      let favorites = [];
      const stub = sinon.stub(factory.songs.shape());

      before(() => {
        stub.findByGenre.returns([]);
        stub.findByArtist.returns([]);
        const user = new User(factory.preferences.none().build(), stub);
        favorites = user.getFavoriteSongs();
      });

      it('should not find any songs', () => {
        favorites.should.deep.equal([]);
      });

      it('should not attempt to find songs by genre', () => {
        stub.findByGenre.should.not.have.been.called;
      });

      it('should not attempt to find songs by artist', () => {
        stub.findByArtist.should.not.have.been.called;
      });

    })

  });

});
