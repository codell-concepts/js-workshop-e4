export default  {

  string() {
    let value = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( let i = 0; i < 5; i++ ) {
      value += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return value;
  },

  number() {
    return Math.floor((Math.random() * 1000) + 1);
  }

};

