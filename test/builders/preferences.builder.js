class Preferences {
  constructor() {
    this.none();
  }

  none() {
    this._genre = '';
    this._artist= '';
    return this;
  }

  genre(value) {
    this._genre = value;
    return this;
  }

  artist(value) {
    this._artist = value;
    return this;
  }

  build() {
    return {
      genre: this._genre,
      artist: this._artist
    };
  }
}

export default Preferences;

