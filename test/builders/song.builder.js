import random from './random';

class Song {
  constructor(){
    this._id = random.number();
    this._title = random.string();
    this._genre = random.string();
    this._artist = random.string();
  }

  from(preferences) {
    this._genre = preferences.genre;
    this._artist = preferences.artist;
    return this;
  }

  genre(value) {
    this._genre = value;
  }

  artist(value) {
    this._artist = value;
  }

  build(){
    return {
      id: this._id,
      title: this._title,
      genre: this._genre,
      artist: this._artist
    }
  }
}

export default Song;
