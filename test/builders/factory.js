import Preferences from './preferences.builder';
import random from './random';
import Song from './song.builder';
import songs from './song-list-shape.builder';

class Factory {

  static get preferences() {
    return new Preferences()
  }

  static get random() {
    return random;
  }

  static get song() {
    return new Song();
  }

  static get songs() {
    return songs;
  }

}

export default Factory;
