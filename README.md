## Boardwalk JavaScript Workshop - Exercise 4

> #### Goals

* testing
* mocha / chai
* sinon

> #### Developer Setup

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

``` bash
# Go to your projects directory
cd wherever-you-store-your-projects

# Clone this repository
git clone https://codell-concepts@bitbucket.org/codell-concepts/js-workshop-e4.git

# Go into the repository
cd js-workshop-e4

# install dependencies
npm install

```

> #### Running exercises

``` bash
# start exercise 1 (ecosystem)
npm run start:e1

# start exercise 2 (es6)
npm run start:e2

# start exercise 3 (promises, async await)
npm run start:e3

# exercise 4 (testing)
npm test

```

> #### Testing

``` bash

# run unit tests once
npm test

# continuous unit testing
npm run test:watch

# test coverage
npm run test:coverage

# linting
npm run lint

```

> #### Building

``` bash

# build to dist
npm run build

# Clean dist
npm run clean

```