> Mocha / chai installation

1. *npm i --save-dev mocha chai chai-as-promised sinon sinon-chai*
2. *npm i --save-dev istanbul nyc*
3. configure mocha
    * setup.js
    * mocha.opts
4. test script packagage.json
    *  "test": "mocha --compilers js:babel-register",
    * "test:watch": "npm test -- --watch",
    * "test:coverage": "node_modules/.bin/nyc npm test",
    * "posttest": "npm run lint"   

> Tests

1. describe, context, it
2. lifecycle: before, after, beforeEach, afterEach
3. chai assertions

> Spies, Stubs, Mocks

1. Sinon
