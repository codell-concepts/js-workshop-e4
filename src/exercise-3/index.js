import chalk from 'chalk';

import User from './user';
import songs from './song-list';
import { play } from './song-player';
import tracker from './song-tracker';

const preferences = { genre: 'alternative', artist: 'Snoop Dogg' };
const user = new User(preferences, songs);

console.log(chalk.red('-- Begin --'));

console.log(chalk.blue('Loading your favorites...'));
user.getFavoriteSongs().then((favorites) => {
  console.log(chalk.blue.bold(`Found ${favorites.length} favorites`));

  console.log(chalk.cyan('Playing your first favorite...'));
  const firstSong = favorites[0];
  play(firstSong);
  console.log(chalk.cyan.bold(`Now playing '${firstSong.title}' by '${firstSong.artist}'`));

  console.log(chalk.yellow('Running song tracking report...'));
  tracker.report();

  console.log(chalk.red('-- End --'));
});

