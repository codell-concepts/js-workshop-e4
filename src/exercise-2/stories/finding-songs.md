## Finding Songs

*As a user*  
*I would like to be able to find songs by genre and artist*  
*so that I can listen to tunes.*

### Acceptance Criteria

**Scenario #1 - Finding songs by genre**

>Given a list of songs  
When asking for songs by a certain genre  
Then should show songs for the specified genre  

**Scenario #2 - Finding songs by artist**

>Given a list of songs  
When asking for songs by a certain artist  
Then should show songs for the specified artist






