## Playing a Song

*As a user*  
*I would like to play a song*  
*so that I can listen to my choice music.*

*As a capitalist pig*  
*I would like to know what songs are being played*  
*so that I only pay royalties for songs users are enjoying.*

### Acceptance Criteria

**Scenario #1 - Song is selected**

>Given a selected song  
When asking to play the song  
Then should play the song  
And should track the song being played

**Scenario #2 - No song is selected**

>Given no song selected  
When asking to play the song  
Then should attempt to play a song  
And should attempt to track a song

