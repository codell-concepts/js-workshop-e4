## Running the App

*As a super user*  
*I would like the app to load my favorites*  
*and play the first song*  
*and run the song tracking report*  
*so that I can be lazy.*

*Assumptions*

* Song list has user preferences
* Super user already signed in

### Acceptance Criteria

>Given a song list  
And the super user has preferences  
When running the app  
Then the app should find the user's favorite songs  
And should play the first song  
And should run the song tracking report  
And should provide feedback what the app is doing*

\* Example output:

*Loading your favorites...*  
*Found {n} favorites*  

*Playing your first favorite song*  
*Now playing '{song title}' by '{song artist}'*

*Running song tracking report...*  
*{Report output}*
