## Generate a Tracking Report

*As a capitalist pig*  
*I would like to know what songs are being played*  
*so that I only pay royalties for songs users are enjoying.*

### Acceptance Criteria

**Scenario #1 - First time song is tracked**

>Given a song has been requested to be tracked for the first time   
When asking to generate a tracking report  
Then the report should indicate the song has been played once

**Scenario #2 - Song has been tracked multiple times**

>Given a song has been requested to be tracked more than once   
When asking to generate a tracking report   
Then the report should indicate the number of times the song has been played