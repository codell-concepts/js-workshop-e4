import * as face from './face';
import nosh from './food';
import libations, { soylant } from './beverages';

nosh.forEach(food => face.eat(food));
face.drink(soylant);
face.sleep();

const faceFuel = [...nosh, ...libations];
faceFuel.forEach(thing => console.log(`${thing.name} is fuel for my face!`));

const uberSoylant = { ...soylant, taste: 'delicious' };
console.log(uberSoylant);

// <script x />
//     var face = ...
// <script y />
//     var food = ...
// <script app />
//     face.drink();

