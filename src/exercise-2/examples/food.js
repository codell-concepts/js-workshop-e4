const banana = { name: 'banana' };
const apple = { name: 'apple' };
const taco = { name: 'taco' };

export default [
  banana,
  apple,
  taco
];
