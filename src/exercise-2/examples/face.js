import mouth from './mouth';
import Eye from './Eye';

export const eat = (food) => {
  mouth.chew(food);
};

export const drink = (beverage) => {
  mouth.drink(beverage);
};

export const sleep = () => {
  const left = new Eye('left', 'blue');
  const right = new Eye('right');
  left.close();
  right.close();
};

export const wake = (makeNoise) => {
  makeNoise();
};

export const makeup = artist => (
  artist.applyLipstick(mouth)
);

export const punch = (hooligan) => {
  hooligan.punch(mouth);
};

