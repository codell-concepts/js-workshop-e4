class Eye {
  constructor(side = 'center', color = 'green') {
    this.side = side;
    this.color = color;
    this.isOpen = true;
  }

  close() {
    this.isOpen = false;
    console.log(`my ${this.side} ${this.color} eye is closed!`);
  }

  open() {
    this.isOpen = true;
  }

  blink() {
    this.isOpen = false;
    setTimeout(() => {
      this.isOpen = true;
      console.log(`I blinked my ${this.side} ${this.color} eye!`);
    }, 500);
  }
}

export default Eye;
