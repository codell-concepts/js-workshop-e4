const chew = (food) => {
  console.log(`I'm chewing a ${food.name}! Yay for me!`);
};

const drink = (beverage) => {
  console.log(`I'm drinking ${beverage.name}! Yay for me!`);
};

export default {
  chew,
  drink
};
