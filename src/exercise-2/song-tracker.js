const played = {};

export default {
  track: ({ id }) => {
    played[id] = played[id] ? played[id] + 1 : 1;
  },

  playCount: song => (played[song.id] ? played[song.id] : 0),

  generateReport: (display) => {
    display(played);
  }
};
