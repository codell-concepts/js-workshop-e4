## Exercise 2 - ES6

* Babel
* import
  * export
  * export
  * import
  * import * as foo
  * destructuring
  * spread - array &amp; object
* arrows
* let vs const vs var
* enhanced literals
* classes
* string templates

#### Examples

*song-list*  
* const
* arrows
* export
* export default
* enhanced literals

*user*
* classes
* array
* spread
* object spread
* export default

*player*  
* import own code

*tracker* 
* import vendor code
* destructuring
* string templates

*index*
* import vendor and own code / module development
* import destructuring
* string literals


