class User {
  constructor(preferences, songs) {
    this._pref = preferences;
    this._songs = songs;
  }

  getFavoriteSongs() {
    return [
      ...(this._pref.genre ? this._songs.findByGenre(this._pref.genre) : []),
      ...(this._pref.artist ? this._songs.findByArtist(this._pref.artist) : [])
    ];
  }

  // updatePreferredGenre(genre) {
  //   // this._preferences.genre = genre;
  //   this._preferences = { ...this._preferences, genre };
  // }
}

export default User;
